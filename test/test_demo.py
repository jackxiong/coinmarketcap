from selenium.webdriver import Chrome
import time
from selenium.webdriver.common.by import By
import csv
from datetime import datetime



from selenium.webdriver.chrome.options import Options
chrome_options = Options()
chrome_options.add_argument("--headless")
driver = Chrome(options=chrome_options,executable_path='../driver/chromedriver')

#pytest test_demo.py::test_coins -s


def test_coins():

    url='https://coinmarketcap.com/'
    
    #page 1
    driver.get(url)
    driver.set_window_size(1667, 1020)
    #get_currencie_historical_data()
    get_currencie_info()

    # currencie_historical_pages =4
    # for  i in range(2,currencie_historical_pages):

    #     time.sleep(3)
    #     p=f'https://coinmarketcap.com/?page={i}'
    #     driver.get(p)
    #     print('go to page',p)
    #     get_currencie_historical_data()

    # currencie_info_pages=6
    # for i in range(2,currencie_info_pages):
    #     time.sleep(3)
    #     p=f'https://coinmarketcap.com/?page={i}'
    #     driver.get(p)
    #     print('go to page',p)
    #     get_currencie_info()


    driver.close()
    print('done....')

def get_currencie_info():
    
    scroll_windows()

    csv_file = "currency_info_" + datetime.now().strftime("%m%d%H%M%S%f") + ".csv"
    header = ["#","Name","Abb","Price", "24h", "7d", "Market Cap", "Volume", "circulating supply", "Last 7 Days"]
    output = open(csv_file, "w")
    writer = csv.writer(output, delimiter=",")
    writer.writerow(header)

    x = '//div[@id="__next"]/div/div[2]/div/div/div[2]/table/tbody/tr'
    els = []
    els = driver.find_elements_by_xpath(x)
    print('els size=>',len(els))

    i=1
    for e in els:
        if "Crypto.com" not in e.text:
            print(i,'=>',e.text)
            row = e.text.split("\n")
            writer.writerow(row)
            print('..............')
        i=i+1

    time.sleep(3)
    output.close()
    print("get_currencie_data ...done!")

def get_currencie_historical_data():

    scroll_windows()

    csv_file = "historical_data_" + datetime.now().strftime("%m%d%H%M%S%f") + ".csv"
    header = ["name","date", "open", "High","Low", "close", "Volume","Market cap"]
    output = open(csv_file, "w")
    writer = csv.writer(output, delimiter=",")
    writer.writerow(header)

    x = '//div[@id="__next"]/div/div[2]/div/div/div[2]/table/tbody/tr'
    els = []
    els = driver.find_elements_by_xpath(x)
    print('els size=>',len(els))
 
    i=1
    l=[]
    for e in els:
        
        if "Crypto.com" not in e.text and i !=10:
            u = f'//*[@id="__next"]/div/div[2]/div/div/div[2]/table/tbody/tr[{i}]/td[3]/a'
            u = driver.find_element_by_xpath(u)
            print(i,'=>',u.get_attribute('href'))
            print('..............')
            h='historical-data/'
            print('go to=>',str(u.get_attribute('href'))+h)
            l.append(str(u.get_attribute('href'))+h)
        i=i+1

    t=1
    for hdurl in l:
        print(t,'hdurl=>',hdurl)
        driver.get(hdurl)
        time.sleep(3)
        
        # select last year
        driver.find_element_by_css_selector('.eZMaTl:nth-child(2)').click()
        time.sleep(2)
        driver.find_element_by_xpath('//li[contains(.,"Last Year")]').click()
        time.sleep(2)
        driver.find_element_by_xpath('//button[contains(.,"Done")]').click()
        time.sleep(2)
        
        #load table
        eles=[]
        hd = '//*[@id="__next"]/div/div[2]/div/div[3]/div/div/div[2]/table/tbody/tr'
        eles=driver.find_elements_by_xpath(hd)
        print('historical -table =>',len(eles))
        line =0
        for e_ in eles:
            print(str(line)+"=>"+e_.text)
            row = []
            e =  hdurl.split("/")
            if line ==0:
                row.append(e[4])
            else:
                row.append("")
            row.append(e_.text[:12])
            row = row + e_.text[13:].split(" ")
            writer.writerow(row)
            line = line+1
        t=t+1
    time.sleep(3)
    output.close()
    print('get_currencie_historical_data ...done!')

def scroll_windows():

    time.sleep(3)
    driver.execute_script("window.scrollTo(0,114)")
    time.sleep(3)
    driver.execute_script("window.scrollTo(0,314)")
    time.sleep(3)
    driver.execute_script("window.scrollTo(0,718)")
    time.sleep(3)
    driver.execute_script("window.scrollTo(0,1116)")
    time.sleep(3)
    driver.execute_script("window.scrollTo(0,2325)")
    time.sleep(3)
    driver.execute_script("window.scrollTo(0,3800)")
    time.sleep(3)
    driver.execute_script("window.scrollTo(0,4406)")
    time.sleep(3)
    driver.execute_script("window.scrollTo(0,6100)")
    time.sleep(3)
    driver.execute_script("window.scrollTo(0,7412)")
    time.sleep(3)

